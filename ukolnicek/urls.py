"""ukolnicek URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls import url
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
    path('account/', include('django.contrib.auth.urls')),

    path('user/overview/<int:user_id>/', views.useroverview, name='useroverview'),
    path('user/detail/<int:user_id>/', views.userdetail, name='userdetail'),
    path('user/create/', views.usercreate, name='usercreate'),
    path('user/edit/<int:user_id>/', views.useredit, name='useredit'),

    path('task/overview/<int:task_id>/', views.taskoverview, name='taskoverview'),
    path('task/detail/<int:task_id>/', views.taskdetail, name='taskdetail'),
    path('task/create/', views.taskcreate, name='taskcreate'),
    path('task/create/<int:group_id>/', views.taskcreate, name='taskcreate'),
    path('task/delete/<int:task_id>/', views.taskdelete, name='taskdelete'),
    path('task/check/<int:task_id>/', views.taskcheck, name='taskcheck'),
    path('task/edit/<int:task_id>/', views.taskedit, name='taskedit'),

    path('group/create/', views.group_create, name='group_create'),
    path('group/detail/<int:group_id>/', views.group_detail, name='group_detail'),
    path('group/delete/<int:group_id>/', views.group_delete, name='group_delete'),
    path('group/overview/<int:group_id>/', views.group_overview, name='group_overview'),
    path('group/edit/<int:group_id>/', views.group_edit, name='group_edit'),

    path('comment/create/<int:task_id>/', views.commentcreate, name='commentcreate'),
    path('comment/delete/<int:comment_id>/', views.commentdelete, name='commentdelete'),
]
