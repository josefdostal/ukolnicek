from django.contrib import admin
from .models import Task,Group,User,UserTask,UserGroup,GroupTask,Comment

# Register your models here.
admin.site.register(Task)
admin.site.register(Group)
admin.site.register(UserTask)
admin.site.register(UserGroup)
admin.site.register(GroupTask)
admin.site.register(Comment)
