from django.contrib.auth import authenticate, login
from django.db.models import Q
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Task, UserTask, Group, UserGroup
from .forms import CreateTask, CreateGroup, EditGroup
from .models import Task, UserTask,Comment,GroupTask
from .forms import CreateTask, EditUserForm, UserCreationForm, EditTaskForm,CreateCommentForm
from django.contrib.auth.models import User as AuthUsers, User
from .forms import UserCreationForm
from django.urls import reverse
import datetime


def home(request):
    return render(request, 'home.html')


# region User
def useroverview(request, user_id):
    if request.user.is_authenticated:
        CurrentUser = get_object_or_404(AuthUsers, pk=user_id)
        tasks = CurrentUser.task_set.all().order_by('deadline')
        # groups = Group.objects.order_by('created_at')
        groups = CurrentUser.group_set.all()
        return render(request, 'user/overview.html', {'tasks': tasks, 'CurrentUser': CurrentUser, 'groups': groups})
    else:
        return render(request, 'error.html')


def usercreate(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect(reverse('useroverview', args=(user.id,)))
    else:
        form = UserCreationForm()
    return render(request, 'user/create.html', {'form': form})


def userdetail(request, user_id):
    if request.user.is_authenticated and user_id == request.user.id:
        user = get_object_or_404(AuthUsers, pk=user_id)
        groups = user.group_set.all()  # FIXME
        tasks = user.task_set.all()  # FIXME
        return render(request, 'user/detail.html', {'user': user, 'groups': groups, 'tasks': tasks})
    else:
        return render(request, 'error.html')


def useredit(request, user_id):
    if request.user.is_authenticated and user_id == request.user.id:
        data={  'email':request.user.email,
                'first_name':request.user.first_name,
                'last_name':request.user.last_name}

        form = EditUserForm(request.POST or None, initial=data)
        if request.method == 'POST' and form.is_valid():
            request.user.first_name = request.POST['first_name']
            request.user.last_name = request.POST['last_name']
            request.user.email = request.POST['email']
            request.user.save()
            return HttpResponseRedirect(reverse('userdetail', args=(request.user.id,)))
        return render(request, "user/edit.html", {"form": form})
    else:
        return render(request, 'error.html')


# endregion User


# region Task
def taskoverview(request, task_id):
    if request.user.is_authenticated:
        task = get_object_or_404(Task, pk=task_id)
        comments = Comment.objects.filter(task_id=task)
        users = task.users.all()
        print(users)
        groups = task.group_set.all()
        print(groups)
        return render(request, 'task/overview.html', {'users': users, 'CurrentTask': task,'comments':comments,'groups': groups})
    else:
        return render(request, 'error.html')


def taskcreate(request,group_id=0):
    if(group_id>0):
        data = {'groups': group_id,
                'deadline': datetime.datetime.now()}
    else:
        data = {'users': request.user.id,
                'deadline': datetime.datetime.now()}
    form = CreateTask(request.POST or None, initial=data)
    if form.is_valid():
        deadline = form.cleaned_data['deadline']

        task = Task.objects.create(
            name=form.cleaned_data['name'],
            description=form.cleaned_data['description'],
            deadline=deadline)

        for user_id in form.cleaned_data['users']:
            user = get_object_or_404(AuthUsers, pk=user_id)
            print("useri: "+str(user)+" id:"+str(user_id))
            UserTask.objects.create(user_id=user, task_id=task, admin=True, deadline=deadline, completed=False)

        for group_id in form.cleaned_data['groups']:
            group = get_object_or_404(Group, pk=group_id)
            print("groupy: "+str(group)+" id:"+str(group_id))
            GroupTask.objects.create(group_id=group, task_id=task, admin=True, deadline=deadline, completed=False)
        return HttpResponseRedirect(reverse('taskdetail', args=(task.id,)))
    return render(request, 'task/create.html', {'form': form})


def taskdetail(request, task_id):
    if request.user.is_authenticated:  # and TODO task je jeho
        task = get_object_or_404(Task, pk=task_id)
        solvers = task.users.all()

        if(task.created_at.strftime("%Y-%m-%d %H:%M:%S") != task.updated_at.strftime("%Y-%m-%d %H:%M:%S")): #pls miliseconds
            updated = True
        else:
            updated = False

        return render(request, 'task/detail.html', {'task': task, 'solvers': solvers,'updated':updated})
    else:
        return render(request, 'error.html')


def taskedit(request, task_id):
    if request.user.is_authenticated:  # and TODO task je jeho
        task = get_object_or_404(Task, pk=task_id)
        data={  'name':task.name,
                'description':task.description,
                'deadline':task.deadline,
                'users':task.users.all()}

        form = EditTaskForm(request.POST or None, initial=data)
        if request.method == 'POST' and form.is_valid():
            task.name = request.POST['name']
            task.deadline = request.POST['deadline']
            task.description = request.POST['description']

            task.users.clear()
            for use in form.cleaned_data['users']:
                UserTask.objects.create(user_id=use, task_id=task, admin=True, deadline=task.deadline,
                                            completed=False)
            task.save()
            return HttpResponseRedirect(reverse('taskdetail', args=(task.id,)))
        return render(request, "task/edit.html", {"form": form, 'task': task})
    else:
        return render(request, 'error.html')


def taskdelete(request, task_id):
    if request.user.is_authenticated:  # and TODO task je jeho
        Task.objects.filter(pk=task_id).delete()
        return HttpResponseRedirect(reverse('useroverview', args=(request.user.id,)))
    else:
        return render(request, 'error.html')


def taskcheck(request, task_id):
    task = Task.objects.get(pk=task_id)
    if task.completed:
        task.completed = False
        task.completed_at = None
    else:
        task.completed = True
        task.completed_at = datetime.datetime.now()
    task.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


# endregion Task

# region Group
def group_create(request):
    data = {'users': request.user.id}
    form = CreateGroup(request.POST or None, initial=data)
    if form.is_valid():
        group = Group.objects.create(
            name=form.cleaned_data['name'],
            description=form.cleaned_data['description'])

        UserGroup.objects.create(user_id=request.user, group_id=group, admin=True, created_at=datetime.datetime.now())
        users = form.cleaned_data['users']
        users = AuthUsers.objects.filter(username__in=users)
        for user in users:
            if user != request.user:
                UserGroup.objects.create(user_id=user, group_id=group, admin=False, created_at=datetime.datetime.now())
        return HttpResponseRedirect(reverse('group_detail', args=(group.id,)))
    return render(request, 'group/create.html', {'form': form})


def group_detail(request, group_id):
    group = get_object_or_404(Group, pk=group_id)
    members = group.users.all()
    tasks = group.tasks.all()
    return render(request, 'group/detail.html', {'group': group, 'members': members, 'tasks': tasks})


# TODO add verification
def group_delete(request, group_id):
    Group.objects.filter(pk=group_id).delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def group_overview(request, group_id):
    if request.user.is_authenticated:
        group = get_object_or_404(Group, pk=group_id)
        users = group.users.all().order_by('username')
        tasks = group.tasks.all().order_by('deadline')
        return render(request, 'group/overview.html', {'users': users, 'CurrentGroup': group, 'tasks': tasks})


def group_edit(request, group_id):
    if request.user.is_authenticated:  # and TODO task je jeho
        group = get_object_or_404(Group, pk=group_id)
        data = {'name': group.name,
                'description': group.description,
                'users': list(group.users.all())}
        form = EditGroup(request.POST or None, initial=data, group=group)
        if request.method == 'POST' and form.is_valid():
            group.name = form.cleaned_data['name']
            group.description = form.cleaned_data['description']
            add_users = form.cleaned_data['add_users']
            rem_users = form.cleaned_data['rem_users']

            add_users = User.objects.filter(username__in=add_users)
            for user in add_users:
                UserGroup.objects.create(user_id=user,
                                         group_id=group,
                                         created_at=datetime.datetime.now(),
                                         admin=False)

            rem_users = User.objects.filter(username__in=rem_users)
            for user in rem_users:
                UserGroup.objects.filter(user_id=user, group_id=group).delete()

            group.save()
            return HttpResponseRedirect(reverse('group_detail', args=(group_id, )))
        return render(request, "group/edit.html", {'form': form, 'group': group})
    return render(request, 'error.html')
# endregion Group

#region Comment
def commentcreate(request,task_id):
    if request.user.is_authenticated:
        form = CreateCommentForm(request.POST or None)
        if form.is_valid():
            task = get_object_or_404(Task, pk=task_id)
            description=form.cleaned_data['description']

            Comment.objects.create(user_id=request.user,task_id=task,description=description)
            return HttpResponseRedirect(reverse('taskoverview', args=(task.id,)))
        return render(request, 'comment/create.html', {'form': form})
    else:
        return render(request, 'error.html')

def commentdelete(request, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    if request.user.is_authenticated and request.user.id == comment.user_id.id:
        Comment.objects.filter(pk=comment_id).delete()
        return HttpResponseRedirect(reverse('taskoverview', args=(comment.task_id.id,)))
    else:
        return render(request, 'error.html')
#endregion Comment