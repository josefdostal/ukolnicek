from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

class Task(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True,blank=True)
    deadline = models.DateTimeField(null=True,blank=True)
    completed_at = models.DateTimeField(null=True,blank=True)
    completed = models.BooleanField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    users = models.ManyToManyField(User, through='UserTask')
    comments = models.ManyToManyField(User, through='Comment', related_name='%(class)s_comment')


class Group(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    users = models.ManyToManyField(User, through='UserGroup')
    tasks = models.ManyToManyField(Task, through='GroupTask')

#M:N
class UserTask(models.Model):
    admin = models.BooleanField()
    completed_at = models.DateTimeField(null=True,blank=True)
    completed = models.BooleanField(null=True,blank=True)
    deadline = models.DateTimeField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)

class UserGroup(models.Model):
    admin = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    group_id = models.ForeignKey(Group, on_delete=models.CASCADE)

class GroupTask(models.Model):
    admin = models.BooleanField()
    completed_at = models.DateTimeField(null=True,blank=True)
    completed = models.BooleanField(null=True,blank=True)
    deadline = models.DateTimeField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    group_id = models.ForeignKey(Group, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)

class Comment(models.Model):
    description = models.TextField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)
