from tokenize import group

from django.db.models import Q
from django.forms import ModelForm, SelectDateWidget, DateField, DateTimeInput, DateTimeField, DateInput, \
    EmailField, CharField, ChoiceField, HiddenInput, BaseForm, Form, forms, Textarea
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import MultipleChoiceField
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from django import forms

from .models import Task,Comment


class CreateTask(ModelForm,CreateView):
    class Meta:
        model = Task
        fields = ['name', 'description','users','deadline']

        error_messages = {
            'name': {
                'max_length': _("Too long"),
            },
        }


def get_users():
    users = User.objects.all().values_list('id','username')
    return tuple(users)

def get_groups():
    groups = Group.objects.all().values_list('id','name')
    return tuple(groups)

class CreateTask(Form):
    def __init__(self, *args, **kwargs):
        super(CreateTask, self).__init__(*args, **kwargs)
        self.fields['groups'] = MultipleChoiceField(choices=get_groups(), label=_('Groups'), required=False)

        self.fields['users'] = MultipleChoiceField(choices=get_users(), label=_('Users'), required=False)

    name = CharField(label=_("Name"), required=True)
    description = CharField(label=_("Description"), required=False,widget=forms.Textarea)
    users = MultipleChoiceField(choices=get_users(), label=_('Users'), required=False)
    groups = MultipleChoiceField(choices=get_groups(), label=_('Groups'), required=False)
    deadline = CharField(label=_("Deadline"), required=True)

class CreateGroup(Form):
    name = CharField(label=_("Group name"), required=True)
    description = CharField(label=_("Description"), required=False)
    users = MultipleChoiceField(choices=get_users(), label=_('Users'), required=False)


def get_rem_users(group):
    users = group.users.all().values_list('username', 'username')
    return users


def get_add_users(group):
    users = []
    for user in group.users.all():
        users.append(User.objects.exclude(pk=user.pk).values_list('username', 'username'))
    return users[0]


class EditGroup(Form, CreateView):
    def __init__(self, *args, **kwargs):
        self.group = kwargs.pop('group')
        super(EditGroup, self).__init__(*args, **kwargs)
        self.fields['name'] = forms.CharField(label='Group name', required=True)
        self.fields['description'] = forms.CharField(label=_("Description"), required=False, widget=Textarea)
        self.fields['add_users'] = forms.MultipleChoiceField(choices=get_add_users(self.group), label=_('Add Users'), required=False)
        self.fields['rem_users'] = forms.MultipleChoiceField(choices=get_rem_users(self.group), label=_('Remove user'), required=False)

    class Meta:
        fields = ('name', 'description', 'add_users', 'rem_users')


class UserCreationForm(UserCreationForm):
    email = EmailField(label=_("Email address"), required=False)
    first_name = CharField(label=_("First name"), required=False)
    last_name = CharField(label=_("Last name"), required=False)

    class Meta:
        model = User
        fields = ("username", "email","first_name","last_name", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        if commit:
            user.save()
        return user

class EditUserForm(ModelForm,CreateView):
    class Meta:
        model = User
        fields = ['first_name', 'last_name','email']

class EditTaskForm(ModelForm,CreateView):
    class Meta:
        model = Task
        fields = ['name','description','deadline','users']

class CreateCommentForm(ModelForm,CreateView):
    class Meta:
        model = Comment
        fields = ['description']
