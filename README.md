# Úkolnicek

<html>
<table>
    <tr>
        <td align="center" colspan="3"><b>INFO</b></td>
    <tr>
        <td><b>Autoři</b><td align="center">Martin Pustka</td><td align="center">Josef Dostál</td>
    </tr>     
    <tr>
        <td><b>Loginy</b><td align="center">PUS0065</td><td align="center">DOS0161</td>
    </tr>     
    <tr>
        <td><b>Rok</b></td><td align="center" colspan="2">2018/2019</td>
    </tr>
    <tr>
        <td><b>Semestr</b></td><td align="center" colspan="2">Zimní</td>
    </tr>
    <tr>
        <td><b>Ročník</b></td><td align="center" colspan="2">2</td>
    </tr>
    <tr>
        <td><b>Předmět</b></td><td align="center" colspan="2">SPJA</td>
    </tr>
</table>
</html>
  
## Popis
Systém bude uživateli poskytovat management úkolů a skupin.  
V Úkolníčku bude moct přihlášený uživatel vytvořit skupinu a přidávat do ní jíné uživatele.  
Mohou také vytvořit úkoly pro uživatele nebo pro skupiny.
Uživatelé mohou přidávat komentář k úkolům, kterých se účastní.  
Dále si budou moct rozkliknout detail kterékoliv skupiny, do které patří,
stejně jako úkolu, který je jejich skupině přiřazen.  
Neregistrovaný uživatel si bude moct vytvořit svůj účet.

## Prvky
### Modely (7)
1. [x] Uživatel
1. [x] Skupina
1. [x] Úkol
1. [x] Komentář (VT)
1. [x] Skupina-Úkol (VT)
1. [x] uživatel-Úkol (VT)
1. [x] Uživatel-Skupina (VT)

### Views (12)
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO
1. [ ] TODO

### Templaty (12)
1. [ ] Úvodní stránka (popis aplikace, možnost registrace nebo přihlášení)
1. [ ] Registrační stránka
1. [ ] Přihlašovací stránka
1. [ ] Editace svého profilu 
1. [ ] Detail uživatele (přehled o uživateli + seznam skupin a úkolů, kterých se účastní)
1. [ ] Detail skupiny (přehled o skupině + seznam uživatelů a úkolů, které seskupuje)
1. [ ] Detail úkolu (přehled o úkolu + seznam uživatelů a skupin, kteří na něm pracují)
1. [ ] Editace skupiny
1. [ ] Editace úkolu
1. [ ] Vytvoření nového úkolu
1. [ ] Vytvoření nové skupiny
1. [ ] Přidání komentáře k úkolu

### Formuláře (6)
1. [ ] Registrace
1. [ ] Přihlášení
1. [ ] Vytvoření nového úkolu
1. [ ] Vytvoření nové skupiny
1. [ ] Přidání komentáře k úkolu
1. [ ] Přidání uživatele,úkolu do skupiny (součást detailu skupiny)
1. [ ] Přidání uživatele,skupiny do úkolu (součást detailu úkolu)
